# Terminology and Graph Pattern Harmonization

Collection of terminologies and process graph patterns collected and decided on by the CopperDigital top-level-ontology group for usage in every other CopperDigital process graph.

The top-level-ontology group wants to meet regularly and decide on common terminology and/or graph patterns which will then be presented in the existing CopperDigital process graph developer meeting as draw.io graph templates.

Topic suggestions for the top-level-ontology group meetings can be put forward in issues using the label "discussion topic"